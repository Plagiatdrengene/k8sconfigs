# Kubernetes config for **SW705E20**


## Assumptions  
  - ### You have a working Ubuntu install 

---
### If you do not already have docker installed and configured, you have to do so by  

```bash
sudo apt-get update && curl -fsSL https://get.docker.com -o get-docker.sh && sudo sh get-docker.sh
sudo usermod -aG docker $USER
```

### Install microk8s

```bash
sudo snap install microk8s --classic
```

### Join the microk8s group

```bash
sudo usermod -a -G microk8s $USER
sudo chown -f -R $USER ~/.kube
```

Start a new session to apply changes `su - $USER`  

---

### Gitlab token

You have to get a token from gitlab to login, in order to pull images from the repo.

To do this go to: https://gitlab.com/-/profile/personal_access_tokens.

Give the token a name and then select read_repository and read_registry.

Lastly press "Create personal access token", you need to use this token as your password when cloning and login to Docker.

Follow the instructions when running:
`docker login registry.gitlab.com`

---

### Clone this repo
Now you need to clone this repo by doing (You need to use your token from before as password):

```bash
git clone https://gitlab.com/swp7/k8sconfigs
```

And then cd into the newly cloned dir.

---
### Make sure microk8s is ready

Run `microk8s status --wait-ready`, and it'll wait until it is ready


## Deploy the basics

  - Run the `initmicro.sh` script, to enable DNS, storage, and metallb (load balancer).


  - Deploy kubeMQ, bu running `kubemq.sh`. You might have to get your own token from [here](https://account.kubemq.io/login/register)

    - **Note:** That if the `kubemq.sh` fails you either have to get your own token or just run it once more.

  - Add a secret that tells kubernetes the crendentials for pulling our private images by running the `gitlabSecret.sh` script.

---
### Linkerd
Since we are using GRPC, which is using HTTP2 and is not natively supported for load balancing, we need [linkerd](https://linkerd.io/) to route traffic to the backends, if we want more than 1 replica.
To set this up, it should be sufficient to run the `./linkerd.sh` script.

After it is installed, you can see the dashboard, on your local pc, by running `$HOME/.linkerd2/bin/linkerd dashboard`. The dashboard should automatically open in your default browser. There is also TONNES of stats/info to be seen in this dashboard, which can be useful for visualizing traffic flow as well as track down errors.

---

## Deploy **all** the stuff 

![POGGERS](https://cdn.betterttv.net/emote/58ae8407ff7b7276f8e594f2/3x.png)

Deploy the rest by
`microk8s.kubectl create -f p7kube/`

To find the address the frontend is running at, run
`microk8s.kubectl get svc`
and find the **EXTERNAL-IP** for the frontend deployment

The service can then be accessed from http://EXTERNAL-IP:5000 

---

### Ingress (AAU server only)

If you are trying to set this up on the server, you also have to set up ingress.

Ingress exposes HTTP and HTTPS routes from outside the cluster to services within the cluster. Traffic routing is controlled by rules defined on the Ingress resource.

Simply `cd ingress` and then `./setupIngress.sh`and it should work. 

Some additional config is needed on AAU server, because their network is VERY restricted. This is, however, out of the scope of this tutorial. Look [here](https://github.com/ubuntu/microk8s/issues/276)

**Note to Rasmus** AAU requires CIDR 10.244.0.0/16 for pods, which has to be set manually in files. Also, you have to change coredns upstream dns to aau's dns server.

---

### File Explanations

- `gitlabSecret.sh`
    - Copies your login infomation from docker to Kubernetes so when it needs to pull images it uses your infomation.

- `initmicro.sh`
    - Enables DNS and binds it to 10.1.1.100 to 10.1.1.110.

- `rollingUpdate.sh`
    - Starts a rolling update for the frontend and backend.

- `scale.sh`
    - Creates 2 replicas of backend and frontend.

- `setImage----.sh`
    - Sets the frontend and backend images to be pulled from that tag (ie. setImageDevelop pulls the images from develop.)

- `kubemq.sh`
    - Sets up kubeMQ.

- `linkerd.sh`
    - Sets up linkerd.

- `linkerd.sh`
    - Sets up linkerd.

- `setupIngress.sh`
    - Sets up ingress on AAU server. This does not easily run on other machines, as a static ip is set, and letsencrypt does not work as the domain sw705e20.aau.dk does not point to your pc, and your pc is (hopefully) not publicly accessible on port 80 and 443.